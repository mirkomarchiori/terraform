provider "aws" {
  region = "eu-west-3"
}

data "aws_ami" "centos_ami" {
  most_recent = true
  owners = ["aws-marketplace"]

  filter {
    name   = "name"
    values = ["*CentOS 7 (x86_64)*"]
  }
}

resource "aws_default_vpc" "default" {
  # (resource arguments)
}

resource "aws_security_group" "tf_rancher_cluster" {
  name        = "tf_rancher_cluster"
  description = "Empty security group identifying the cluster"
  #vpc_id      = "${aws_vpc.main.id}"
}

resource "aws_security_group" "tf_rancher_server" {
  name        = "tf_rancher_server"
  description = "Security group for the rancher server"
  #vpc_id      = "${aws_vpc.main.id}"

  ingress {
    description = "Allow communication from within the rancher cluster"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    security_groups = ["${aws_security_group.tf_rancher_cluster.name}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "tf_rancher_nodes" {
  name        = "tf_rancher_nodes"
  description = "Security group for the rancher nodes"
  #vpc_id      = "${aws_vpc.main.id}"

  ingress {
    description = "Allow communication from within the rancher cluster"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    security_groups = ["${aws_security_group.tf_rancher_cluster.name}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "tf_rancher_server" {
  ami = data.aws_ami.centos_ami.id
  instance_type = "t3a.medium"
  security_groups = ["${aws_security_group.tf_rancher_cluster.name}","${aws_security_group.tf_rancher_server.name}"]
  key_name = "ssh-mirdev-milan-linux"
}

resource "aws_instance" "tf_rancher_cp_etcd_worker" {
  ami = data.aws_ami.centos_ami.id
  instance_type = "t3a.medium"
  security_groups = ["${aws_security_group.tf_rancher_cluster.name}","${aws_security_group.tf_rancher_nodes.name}"]
  key_name = "ssh-mirdev-milan-linux"
}
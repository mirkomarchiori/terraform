provider "aws" {
  region = "eu-south-1"
}

resource "aws_vpc" "main" {
  cidr_block = "172.1.0.0/16"
  tags = {
    Name = "vpc-terraform-test"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "main" {
  vpc_id = aws_vpc.main.id
  cidr_block = "172.1.32.0/24"
  availability_zone = "eu-south-1a"
}

resource "aws_route_table" "default" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

/*resource "aws_route_table_association" "main" {
  subnet_id = aws_subnet.main.id
  route_table_id = aws_route_table.default.id
}*/

resource "aws_security_group" "tf_rancher_cluster" {
  name        = "tf_rancher_cluster"
  description = "Empty security group identifying the cluster"
  vpc_id      = aws_vpc.main.id
}

resource "aws_security_group" "tf_rancher_server" {
  name        = "tf_rancher_server"
  description = "Security group for the rancher server"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    security_groups = ["${aws_security_group.tf_rancher_cluster.id}"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "tf_rancher_nodes" {
  name        = "tf_rancher_nodes"
  description = "Security group for the rancher nodes"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.tf_rancher_cluster.id}"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "centos" {
  most_recent = true
  owners = ["769304176199", "aws-marketplace"] # CentOS
  #name_regex = "CentOS Linux 7 x86_64*"
  #image_id = "ami-03014b98e9665115a"

  filter {
    name = "name"
    #name = "image_id"
    #values = ["*CentOS Linux 7*"]
    values = ["CentOS Linux 7 x86_64 HVM EBS ENA 2002_01-b7ee8a69-ee97-4a49-9e68-afaee216db2e-ami-0042af67f8e4dcc20.4"]
    #values = ["ami-03014b98e9665115a"]
  }
}

resource "aws_key_pair" "terraform_aws" {
  key_name   = "terraform_aws"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDfNeJPL44dchxWj/T+mYqqREPPJ0IiW4KL7HCtKUE6/+7AiDMcgN7/ebaE9yrHHA1S6YVQjEv773fR11tdWyWhr84SwCwwmQSrqA8sy9XxXgz1w/WAJQ9eHrAOH/3SSYzaU4TEprFHyI16G5FXtBhnYbrv+kkqJO1/YnQe8KRFkQ== ciori@localhost.localdomain"
}

resource "aws_instance" "tf_rancher_server" {
  ami = data.aws_ami.centos.id
  availability_zone = "eu-south-1a"
  instance_type = "t3.medium"
  vpc_security_group_ids = [aws_security_group.tf_rancher_cluster.id, aws_security_group.tf_rancher_server.id]
  key_name = aws_key_pair.terraform_aws.key_name
  subnet_id = aws_subnet.main.id
  associate_public_ip_address = true
  tags = {
    Name = "tf-rancher-server"
  }
}

resource "aws_instance" "tf_rancher_cp_etcd_worker" {
  ami = data.aws_ami.centos.id
  availability_zone = "eu-south-1a"
  instance_type = "t3.medium"
  vpc_security_group_ids = [aws_security_group.tf_rancher_cluster.id, aws_security_group.tf_rancher_nodes.id]
  key_name = aws_key_pair.terraform_aws.key_name
  subnet_id = aws_subnet.main.id
  associate_public_ip_address = true
  tags = {
    Name = "tf-rancher-node"
  }
}

output "public_ip" {
  value = aws_instance.tf_rancher_server.ami
}